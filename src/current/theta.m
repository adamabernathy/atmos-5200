function [y] = theta ( T, p )

% in:        T = absolute temperature (K)
%            p = pressure (Pa)
% out:   theta = potential temperature (K)

cp = 1004.0;
rgas = 287.00;
pzero = 100000.;

pi = ( p ./ pzero ) .^( rgas / cp );
y = T ./ pi;

      return 
      end
