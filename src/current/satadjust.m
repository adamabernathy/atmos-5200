function [th1, qv1, qc1, qvs1, pibar] = satadjust(th, qv, qc, pbar)
%
%  Performs an isobaric moist adiabatic adjustment.
%  The final state is either subsaturated with no liquid water
%  present, or exactly saturated with liquid water present.
%
%  This version iterates to obtain the adjustment, so accurate
%  guesses for TH and QV are not needed if ITTMAX=10. 
%
%  Units:  SI (MKS)
% 
%  Input -- 
%  TH: potential temperature, theta^* (K)
%  QV: mixing ratio of water vapor, q_v^* (kg/kg)
%  QC: mixing ratio of liquid water, q_c^* (kg/kg)
%  PBAR: pressure, p (Pa)
%
%  Output -- 
%  TH1: adjusted potential temperature, theta^{n+1} (K)
%  QV1: adjusted mixing ratio of water vapor, q_v^{n+1} (kg/kg)
%  QC1: adjusted mixing ratio of liquid water, q_c^{n+1} (kg/kg)
%  QVS1: saturation mixing ratio, q_vs^{n+1} (kg/kg) for TH1, PBAR
%  PIBAR: Exner function, pi (non-dimensional pressure)
%
% Required functions:
%  esat_Pa(T): saturation vapor pressure (Pa)
%  desdT_Pa(T): derivative of esat(T) (Pa/K)

%  physical constants
hlf = 2.500E+06;
cp = 1004;
rgas = 287;
pzero = 100000;

% iteration parameters
ittmax = 10;        % maximum number of iterations allowed
dT_crit = 0.001;    % stop iterating once T adjustment is less than dT_crit 

%  PBAR is a (hydrostatic) reference pressure field

itt = 1;
pibar = (pbar/pzero)^(rgas/cp);
gamma = hlf/(cp*pibar);

thstar = th;
qvstar = qv;
qw = qv+qc;

do_it = 1;
while (do_it == 1)

tstar = thstar*pibar;
es1 = esat_Pa(tstar);

alpha = desdT_Pa(tstar)*0.622*pibar*pbar/(pbar-es1)^(2);
thfac = gamma/(1+gamma*alpha);

qvsat = 0.622./(pbar-es1)*es1;
th1 = thstar+thfac*(qvstar-qvsat);
qv1 = qvsat+alpha*(th1-thstar);
qc1 = qw-qv1;

qvs1 = qv1;

if (qc1 < 0)
  qc1 = 0;
  qv1 = qw;
  th1 = thstar+gamma*(qvstar-qv1);
  qvs1=qvsat+alpha*(th1-thstar);
end

% T adjustment for this iteration
dT = (th1-thstar)*pibar;

if (abs(dT) < dT_crit) || (itt == ittmax)
  do_it = 0;
end

thstar = th1;
qvstar = qv1;

itt = itt+1;

end

