%
%-----------------------------------------------------------------------
%
%	Two-Dimension Parcel Model
%
%  This program has been moved to GitLab repo for code management.
%  See commits for more revision information.
%
%  Adam C. Abernathy, adam.abernathy@gmail.com
%  
%  Repo URL:
%  git@gitlab.com:adam_abernathy/atmos-5200.git
%
%  Indended purpose: Homework 2-2A
%
%-----------------------------------------------------------------------
%

   clear all;
   close all;
   %clc;

   fprintf('\n2-D Cloud Parcel Model\n')

%-----------------------------------------------------------------------
%  KICK OFF
%-----------------------------------------------------------------------

%  Plot results?
   do_plot_1 = 1;       % Temp,Theta vs. P
   do_plot_2 = 1;       % Qc,Qv,Qw vs. P
   do_plot_3 = 1;       % Temp,RH vs. Height, Skew-T

   output_dir = 'hw2_2A_plots';

%  Initial parcel properties
   pMB = 1000.;         % pressure (mb)
   TC = 16;             % temperature (deg C)
   qv = 14.8e-3;        % water vapor mixing ratio 
   qc = 0.;             % liquid water mixing ratio
   qw = 10.0e-3;
   qvs = 10.0e-3;
   rh_i = 0.5;
   lambda = 0;
      
%  Ascent parameters   
   dpMB = 10.;          % pressure interval (mb)
   ptopMB = 250.;       % ending pressure (mb)

%
%  Crossing the Rubicon... We will convert all units to SI, prepare
%  variable arrays and then move on to computing the parcel at
%  various levels of pressure.
%
      
%	Convert units to SI
   t_ice     = 273.15;  % constant
   pa_per_mb = 100.0;   % constant
   kC        = 2*10^-2; % constant, thermodynamic

   p         = pMB * pa_per_mb;
   T         = TC + t_ice;
   dp        = dpMB  * pa_per_mb;
   ptop      = ptopMB * pa_per_mb;

%  Input for adjust: TH, QV, QC
   th = theta(T,p);
   %th = 16;
      
%  Set up column arrays for storing results
   n_cycles = ((pMB-ptopMB)/dpMB);

   p_mb     = zeros(2*n_cycles,1);
   th_K     = zeros(2*n_cycles,1);
   T_K      = zeros(2*n_cycles,1);
   qv_gkg   = zeros(2*n_cycles,1);
   qc_gkg   = zeros(2*n_cycles,1);
   qw_gkg   = zeros(2*n_cycles,1);

   rh       = zeros(2*n_cycles,1);
   qvs_gkg  = zeros(2*n_cycles,1);

%  Store initial values (assuming no adjustment req'd)
   i = 1;

   p_mb(i)   = pMB;
   th_K(i)   = th;
   T_K(i)    = T;

   qv_gkg(i) = qv * 1.e3;
   qc_gkg(i) = qc * 1.e3;
   qv_gkg(i) = qvs;
   rh(i)     = rh_i;

%  T (K) and p (Pa) in environment at initial height, use Std ATM.
   [Te pe]   = TP_std(0);	
   the       = theta(Te,p);	% Kelvin
      
   
%
%  Text output table
%

%  Print unit table & initial conditions.
   fprintf('\n\nParameters & Units\n\n')
   fprintf('Var\tDescription\t\tUnit\n')
   fprintf('----------------------------------------------\n')
   fprintf('P\tPressure\t\t[mb]\n')
   fprintf('TH\tTheta\t\t\t[K]\n')
   fprintf('T_{C}\tTemperature\t\t[C]\n')
   fprintf('Q_{v}\tWater Vap Mix Rat.\t[g*kg^-1]\n')
   fprintf('Q_{c}\tLiq Water Mix Rat.\t[g*kg^-1]\n')
   fprintf('Q_{vs}\t PUT SOMETHING HERE\t[g*kg^-1]\n')
   fprintf('----------------------------------------------\n')

   fprintf('\nINITIAL CONDITIONS...\n')
   fprintf('T_{C}: %3.2f\n',TC)
   fprintf('Q_{v}: %3.2f\n',qv)
   fprintf('Q_{c}: %3.2f\n',qc)
   fprintf('RH: %3.2f\n',rh(1))

   fprintf('(*) Denotes initial conditions\n')


   fprintf('\nCOMPUTATIONS...\n\n')

   fprintf('P\tTH\tT\tqv\tqc\tqvs\n')
   fprintf('----------------------------------------------\n')

   fprintf('*%4.0f\t%3.2f\t%3.2f\t%2.3f\t%2.3f\t%2.3f\t%1.4f\t%1.2f\n',...
            p_mb(i),th_K(i),T_K(i),qv_gkg(i),qc_gkg(i),qvs_gkg(i),rh(i))

%-----------------------------------------------------------------------
%  Ascent & Descend the parcel. We will move it up and down through
%  the atmosphere as a function of pressure.
%-----------------------------------------------------------------------  
      
      % print decend divider, internal use only.
      flag_1 = 0;

      for i=1:2*(n_cycles)
         
%  Check to see if you need to decend... This assumes that
%  we are bringing the parcel back to the starting point
%  in even incriments.
      
       if (i <= n_cycles)
         p=p-dp;
      else
         
         if flag_1 == 0
            fprintf('----------------------------------------------\n')
            fprintf('                    DECENDING                 \n')
            fprintf('----------------------------------------------')
         end
         
         flag_1 = 1;
         p=p+dp;
      end
      
%  For dry adiabatic ascent: TH, QV and QC do not change. 
%  The 'satadjust' performs an isobaric saturation adjustment
      
   %th=th-lambda*(th-the)*dz
   th=th-lambda*(th-the)*dpMB;
   [th qv qc qvs pi] = satadjust(th,qv,qc,p);

   p_mb(i) = p/pa_per_mb;
   th_K(i) = th;
   T_K(i) = th*pi;
   qv_gkg(i) = qv * 1.e3;
   qc_gkg(i) = qc * 1.e3;
   qvs_gkg(i) = qvs * 1.e3;
             
   qw_gkg(i) = (qc*1.e3)+(qv*1.e3);
   rh(i) = qv_gkg(i)/qw_gkg(i);
   %rh(i) = rh(i)/1.e3;

%  print computaions     
   fprintf('\n%4.0f\t%3.2f\t%3.2f\t%2.3f\t%2.3f\t%2.3f\t%1.4f\t%1.2f\n',...
            p_mb(i),th_K(i),T_K(i),qv_gkg(i),qc_gkg(i),qvs_gkg(i),rh(i))
      

%  Break the lines up a bit for the user
      r=5;
      if (mod(i,r) == 0)
         fprintf('\n')
      end
        
%  End accent loop
      end
      
      
%  Indicate the data is over.
      fprintf('\n----------------------------------------------\n')
       

%-----------------------------------------------------------------------
%  PLOT THE OUTPUT
%-----------------------------------------------------------------------

%
%  Plot both theta and T_C on the same plot 
%
   if (do_plot_1 == 1)
                   
      f = '../../plots/hw_2_2a_p1.png';
      fig1 = figure(1);
      box on;
      hold on;
      plot(T_K,p_mb,'-k')
      plot(th_K,p_mb,'--k')

      title('T_{C}(P) and \Theta_{C}(P) [C]','FontSize',14)
      xlabel('TEMPERATURE [C]','FontSize',12)
      ylabel('PRESSURE [mb]','FontSize',12)

      set(gca,'YDir','reverse');
      legend('T_{C}(P)','\Theta_{C}(P)')
      set(gcf,'PaperPositionMode','auto')
      set(fig1, 'Position', [0 0 500 500])

      hold off;

      print(fig1,'-dpng',f)

   end
 
        
%
% Plot all Qx values on same plot
%
   if (do_plot_2 == 1)

      
      f = '../../plots/hw_2_2a_p2.png';
      fig2=figure(2);
      box on
      hold on

      plot(qv_gkg,p_mb,'-k')
      plot(qc_gkg,p_mb,'+k')
      plot(qw_gkg,p_mb,'--k')

      title('Q_{v},Q_{c},Q_{w}  [g*kg^{-1}]','FontSize',14)
      xlabel('[g*kg^{-1}]','FontSize',12)
      ylabel('PRESSURE [mb]','FontSize',12)


      set(gca,'YDir','reverse');
      legend('Q_{v}','Q_{c}','Q_{w}')
      set(gcf,'PaperPositionMode','auto')
      set(fig2, 'Position', [0 0 500 500])

      hold off;

      print(fig2,'-dpng',f)


   end    
      
      
%
%  Skew-T plot
%
   if (do_plot_3 == 1)     
      f = '../../plots/hw2_2a_p3.png';
      fig3 = figure(3);
      tskew(p_mb,T_K-t_ice,rh);
      hold on
      
      %tskew(p_mb,T_K-t_ice,qc_gkg);
      hold off
      print(fig3,'-dpng',f)
   end
        

%  All done!
   fprintf('\n\nComplete.\n\n')