%	program parcel

   clear all;
   close all;
   clc;

%	Constants
   TICE      = 273.15;
   pa_per_mb = 100.;

%-----------------------------------------------------------------------
%	Initial Parcel Properties
%	environment at initial height (NEW)
   z = 0;               % * initial height of parcel (m) 
   [Te pe] = TP_std(z);	% T (K) and p (Pa) in environment at initial height
   RHe1    = 0.8;       % relative humidity in environment at all heights

%	parcel properties at initial height
   thC = Te - TICE;     % potential temp (C)
   qv  = 14.8e-3;       % mixing ratio (kg/kg) 
   qc  = 0;             % * liquid water mixing ratio (kg/kg)
   p   = pe;            % * pressure (Pa)
   th  = theta(Te,p);   % potential temperature (K)
   T   = Te;            % temperature (K)
%-----------------------------------------------------------------------

%	For entraining parcel  (NEW)
   dz   = 100.0;
   ztop = 10.e3;

%	set up column arrays for storing results
   max_size = 101;%1000;

   p_mb     = zeros(max_size,1)*NaN;
   th_K     = zeros(max_size,1)*NaN;
   T_C      = zeros(max_size,1)*NaN;
   qv_gkg   = zeros(max_size,1)*NaN;
   qc_gkg   = zeros(max_size,1)*NaN;      
   
   qvs_gkg  = zeros(max_size,1)*NaN; 
   Te_C     = zeros(max_size,1)*NaN;
   RHe      = zeros(max_size,1)*NaN;

%	store initial values (assuming no adjustment req'd)
   i = 1;
   p_mb(i)    = p/pa_per_mb;     % *
   th_K(i)    = th;              % *
   T_C(i)     = T - TICE;        % *
   qv_gkg(i)  = qv * 1.e3;       % *
   qc_gkg(i)  = qc * 1.e3;       % *

%	(NEW)      
   qvs_gkg(i) = 0.8;          % parcel saturation mixing ratio (g/kg)      
   Te_C(i)    = Te - TICE;    % temperature in environment (C)
   RHe(i)     = RHe1;         % relative humidity in environment

%  Display initial values
   fprintf('P(mb)\tZ\tTH\tT\tq_v\tq_c\n')
   fprintf('--------------------------------------------\n')
   fprintf('%4.0f\t%5.0f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\n',...
           p_mb(i),z,th_K(i)-TICE,T_C(i),qv_gkg(i),qc_gkg(i))

%  For entraining parcel 

   while (z < ztop)	% (NEW)

   i = i + 1;  
   z = z + dz;		% (NEW)

%	Environment T, p (US Standard Atmosphere)	% (NEW)
   [Te pe] = TP_std(z);	% T (K) and p (Pa) in environment at height z

   p = pe;		% parcel pressure = environment pressure

%	Environment theta, qv, qc (NEW)
   the = theta(Te,p);            % (K)
   qve = qv_gkg(i-1) / 1.e3;     % (kg/kg)
   qce = 0;		

%  FOR DRY ADIABATIC ASCENT TH, QV, QC DO NOT CHANGE
%  entrainment 
   th = th_K(i-1);   % !!! suspect
   qv = qv_gkg(i-1) / 1.e3;
   qc = qc_gkg(i-1) / 1.e3;      

%  ADJUST PERFORMS ISOBARIC SATURATION ADJUSTMENT
   [th qv qc qvs pi] = satadjust(th,qv,qc,p);

   p_mb(i)    = p/pa_per_mb;        % *
   th_K(i)    = th;                 % *
   T_C(i)     = (th * pi) - TICE;   % *
   qv_gkg(i)  = qv * 1.e3;          % *
   qc_gkg(i)  = qc * 1.e3;          % *

%  (NEW)      
   qvs_gkg(i) = qv_gkg(i) / RHe(1);        % parcel sat. mix rat. (g/kg) 
   Te_C(i)    = (the * pi) - TICE;         % temp in environment (C)
   RHe(i)     = (qve * 1.e3) / qvs_gkg(i); % RH in environment

%  Display  values       
   fprintf('%4.0f\t%5.0f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\n',...
           p_mb(i),z,th_K(i)-TICE,T_C(i),qv_gkg(i),qc_gkg(i))


   end   % End while

%
%  Plot
%

%  need to correct the first value, this is just for plotting
   T_C(1) = T_C(2);
   qv_gkg(1) = qv_gkg(2);
   qvs_gkg(1) = qvs_gkg(2);
   RHe(1) = RHe(2);
   
   tskew_m(p_mb,T_C,(qv_gkg./qvs_gkg));   	% parcel, MAGENTA
   hold on
   tskew(p_mb,Te_C,RHe);                  % environment
   axis([-20 20 250 1020])                % zoom in

   orient landscape
   print -dpng '../../plots/hw2_2b.png'
