%     program parcel
%     9-9-08 -- changed th_K(1) to th_K(i)
%     11-8-14 -- Lots of updates.

%  Scrub
      clear all;
      close all;
      %clc;
      
%  Welcome message
      fprintf('\n2-D Cloud Parcel Model\n')

%***********************************************************************
%  INITILIZATION PARAMETERS
%***********************************************************************

%     PLOT RESULTS
      do_plot_1 = 1;
      do_plot_2 = 1;

%     CONVERT CLOUD WATER
%     This option is for HW1-C.
      convert_qc = 1;

%     INITIAL PARCEL PROPERTIES
      pMB = 1000.;      % pressure (mb)
      TC = 20;          % temperature (deg C)
      qv = 14.8e-3;     % water vapor mixing ratio 
      qc = 0.;          % liquid water mixing ratio
      qw = 14.8e-3;
      qvs = 0;
      rh_i = 0.5;
      lambda = 0;

%     ASCENT PARAMETERS   
      dpMB = 10.;       % pressure interval (mb)
      ptopMB = 250.;    % ending pressure (mb)


%***********************************************************************
%  CROSSING THE RUBICON, NO OPTIONS PAST THIS POINT
%***********************************************************************

%     CONVERT TO SI UNITS
      TICE = 273.15;
      pa_per_mb = 100.;

      p = pMB * pa_per_mb;
      T = TC + TICE;
      dp = dpMB  * pa_per_mb;
      ptop = ptopMB * pa_per_mb;

%     INPUT FOR ADJUST: TH, QV, QC
      th = theta (T,p);  % theta(T,p) is a function

%     set up column arrays for storing results
      n_cycles = ((pMB-ptopMB)/dpMB);
      
      p_mb = zeros(2*n_cycles,1);
      th_K = zeros(2*n_cycles,1);
      T_K = zeros(2*n_cycles,1);
      qv_gkg = zeros(2*n_cycles,1);
      qc_gkg = zeros(2*n_cycles,1);
      qw_gkg = zeros(2*n_cycles,1);
      rh = zeros(2*n_cycles,1);
      qvs_gkg = zeros(2*n_cycles,1);

%     store initial values (assuming no adjustment req'd)
      i = 1;
      p_mb(i) = pMB;
      th_K(i) = th;
      T_K(i) = T;
      qv_gkg(i) = qv;
      qc_gkg(i) = qc;
      qw_gkg(i) = qw;
      rh(i) = rh_i;
      qvs_gkg(i) = qvs;
      
%  T (K) and p (Pa) in environment at initial height, use Std ATM.
      [Te pe] = TP_std(0);	
      the = theta(Te,p);		% (K)
      
      
%***********************************************************************
%  COMPUTATIONS & TEXT OUTPUT
%***********************************************************************

%     print unit table
      fprintf('\n\nParameters & Units\n\n')
      fprintf('Var\tDescription\t\tUnit\n')
      fprintf('------------------------------------------\n')
      fprintf('P\tPressure\t\t[mb]\n')
      fprintf('TH\tTheta\t\t\t[K]\n')
      fprintf('T_{C}\tTemperature\t\t[C]\n')
      fprintf('Q_{v}\tWater Vap Mix Rat.\t[g*kg^-1]\n')
      fprintf('Q_{c}\tLiq Water Mix Rat.\t[g*kg^-1]\n')
      fprintf('------------------------------------------\n')
      
      fprintf('\nINITIAL CONDITIONS...\n')
      fprintf('T_{C}: %3.2f\n',TC)
      fprintf('Q_{v}: %3.2f\n',qv)
      fprintf('Q_{c}: %3.2f\n',qc)
      
      
      fprintf('\nCOMPUTATIONS...\n\n')
      
      fprintf('P\tTH\tT\tqv\tqc\n')
      fprintf('------------------------------------------\n')

      fprintf('%4.0f\t%3.2f\t%3.2f\t%2.3f\t%2.3f\t%2.3f \n',...
               p_mb(i),th_K(i),T_K(i),qv_gkg(i),qc_gkg(i))

%***********************************************************************
%  ASCEND & DECEND THE PARCEL
%***********************************************************************
      
      % print decend divider
      flag_1 = 0;

      for i=2:2*(n_cycles)
         
      % Check to see if you need to decend... This assumes that
      % we are bringing the parcel back to the starting point
      % in even incriments.
      if (i <= n_cycles)
         p=p-dp;
      else
         
         if flag_1 == 0
            fprintf('------------------------------------------\n')
            fprintf('                DECENDING                 \n')
            fprintf('------------------------------------------')
         end
         
         flag_1 = 1;
         p=p+dp;
      end
      
%  FOR DRY ADIABATIC ASCENT TH, QV, QC DO NOT CHANGE
%  ADJUST PERFORMS ISOBARIC SATURATION ADJUSTMENT
      [th qv qc qvs pi] = satadjust(the,qv,qc,p);

      p_mb(i) = p/pa_per_mb;
      th_K(i) = th;
      
      T_K(i) = th*pi;
      
      qv_gkg(i) = qvs * 1.e3;
      qc_gkg(i) = qc  * 1.e3;
      qw_gkg(i) = qw  * 1.e3;
      
      rh=qv_gkg(i)/(qvs*1.e3);
      
%  ONLY FOR HW1 Q1-C
      if (convert_qc == 1)
         qc_gkg(i) = 0;       
         qw_gkg(i) = (qc*1.e3) + (qv*1.e3);
         
      end
%  print computaions     
      fprintf('\n%4.0f\t%3.2f\t%3.2f\t%2.3f\t%2.3f\t%2.3f \n',...
               p_mb(i),th_K(i),T_K(i),qv_gkg(i),qc_gkg(i))
      

%  Break the lines up a bit for the user
      r=5;
      if (mod(i,r) == 0)
         fprintf('\n')
      end
        
%  End accent loop
      end
      
      
%  Indicate the data is over.
      fprintf('\n------------------------------------------\n')
       

%***********************************************************************
%  PLOT THE OUTPUT
%***********************************************************************
      
%     Does user want to plot?
      if (do_plot_1 == 1)
         
%          figure(1) 
%          subplot(1,2,1) % first subplot
%          box on
%          plot(T_K,p_mb,'-k')
%          title('T_{C}(P)   [C]','FontSize',12)
%          xlabel('Temperature [C]')
%          ylabel('Pressure [mb]')
%          set(gca,'YDir','reverse');
%          
%          
%          subplot(1,2,2) % second subplot
%          plot(th_K,p_mb,'-k')
%          box on
%          title('\Theta_{C}(P)   [C]','FontSize',12)
%          xlabel('Temperature [C]')
%          set(gca,'YDir','reverse');
         
         
%  Plot both theta and T_C on the same plot, export to PNG         
         if (convert_qc == 1)
            f = 'theta_and_t_vs_p_C.eps';
         else
            f = 'theta_and_t_vs_p.eps';
         end

         fig2=figure(2);
         box on;
         hold on;
         plot(T_K,p_mb,'-k')
         plot(th_K,p_mb,'--k')
         
         title('T_{C}(P) and \Theta_{C}(P) [C]','FontSize',14)
         xlabel('Temperature [C]','FontSize',12)
         ylabel('Pressure [mb]','FontSize',12)
         
         set(gca,'YDir','reverse');
         legend('T_{C}(P)','\Theta_{C}(P)')
         ylim([ptopMB,pMB]);
         
         set(gcf,'PaperPositionMode','auto')
         set(fig2, 'Position', [0 0 500 500])
    
         hold off;
         
         f = sprintf(f);
         print(fig2,'-deps2',f)
                
      end
         
      if (do_plot_2 == 1)
         
         % Qx values in subplots.
%          figure(3)
%          subplot(1,3,1)
%          box on
%          plot(qv_gkg,p_mb,'-k')
%          title('Q_{v}   [g*kg^{-1}]','FontSize',12)
%          xlabel('Q_{v} [g*kg^{-1}]')
%          ylabel('Pressure [mb]')
%          set(gca,'YDir','reverse');
%          
%          subplot(1,3,2)
%          box on
%          plot(qc_gkg,p_mb,'-k')
%          title('Q_{c}   [g*kg^{-1}]','FontSize',12)
%          xlabel('Q_{c} [g*kg^{-1}]')
%          ylabel('Pressure [mb]')
%          set(gca,'YDir','reverse');
%          
%          subplot(1,3,3)
%          box on
%          plot(qw_gkg,p_mb,'-k')
%          title('Q_{w}   [g*kg^{-1}]','FontSize',12)
%          xlabel('Q_{w} [g*kg^{-1}]')
%          ylabel('Pressure [mb]')
%          set(gca,'YDir','reverse');
         
         
         % Plot all Qx values on same plot and save as PNG
         if (convert_qc == 1)
            f = 'qx_vs_p_C.eps';
         else
            f = 'qx_vs_p.eps';
         end

         fig4=figure(4);
         box on
         hold on
         
         plot(qv_gkg,p_mb,'-k')
         plot(qc_gkg,p_mb,'+k')
         plot(qw_gkg,p_mb,'--k')
         
         title('Q_{v},Q_{c},Q_{w}  [g*kg^{-1}]','FontSize',14)
         xlabel('Saturation in [g*kg^{-1}]','FontSize',12)
         ylabel('Pressure [mb]','FontSize',12)
         
         
         set(gca,'YDir','reverse');
         legend('Q_{v}','Q_{c}','Q_{w}')
         ylim([ptopMB,pMB]);
         set(gcf,'PaperPositionMode','auto')
         set(fig4, 'Position', [0 0 500 500])
    
         hold off;
         f = sprintf(f);
         print(fig4,'-deps2',f)
         
         
      end    
        
         
      fprintf('\n\nComplete.\n\n')


%  All done!